# qssi-user 12
12 SKQ1.220303.001 V13.1.1.0.SKXINXM release-keys
- manufacturer: xiaomi
- platform: msmnile
- codename: nabu
- flavor: qssi-user
- release: 12
12
- id: SKQ1.220303.001
- incremental: V13.1.1.0.SKXINXM
- tags: release-keys
- fingerprint: Xiaomi/nabu_in/nabu:12/RKQ1.200826.002/V13.1.1.0.SKXINXM:user/release-keys
- is_ab: true
- brand: Xiaomi
- branch: qssi-user-12
12-SKQ1.220303.001-V13.1.1.0.SKXINXM-release-keys
- repo: xiaomi_nabu_dump
